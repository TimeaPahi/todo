# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from datetime import datetime
from django.contrib.auth.models import User
# create a new function to fill the DB with testvalues


def fill_database(apps, schema_editor):
    entry = apps.get_model('todolist', 'Entry')
    userobj = apps.get_model('auth', 'user')

    User.objects.create_superuser('superuser','is131322@fhstp.ac.at','superuser')
    user = userobj.objects.get(id=1)

    User.objects.create_user('testuser','is131322@fhstp.ac.at','testuser')
    user = userobj.objects.get(id=2)

    entry(
        user_id=user,
        title='Erster Eintrag',
        description='1. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()
    entry(
        user_id=user,
        title='Zweiter Eintrag',
        description='2. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()
    entry(
        user_id=user,
        title='Dritter Eintrag',
        description='3. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()

# call the funtion 'fill_database' in Migration


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0001_initial'),
    ]

    operations = [migrations.RunPython(fill_database), ]
