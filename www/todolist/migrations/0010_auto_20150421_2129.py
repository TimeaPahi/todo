# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0009_auto_20150421_2125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='due_date',
            field=models.DateField(default=datetime.datetime(2015, 4, 21, 21, 28, 50, 727549, tzinfo=utc), blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(default=datetime.datetime(2015, 4, 21, 21, 29, 5, 558925, tzinfo=utc), blank=True),
            preserve_default=False,
        ),
    ]
