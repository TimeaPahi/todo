# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0006_auto_20150421_2031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='due_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(null=True),
        ),
    ]
