# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0007_auto_20150421_2116'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='due_date',
        ),
        migrations.RemoveField(
            model_name='entry',
            name='due_time',
        ),
    ]
