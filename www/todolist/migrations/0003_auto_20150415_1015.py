# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.auth.models import User

def fill_database(apps, schema_editor):
    entry = apps.get_model('todolist', 'Entry')
    userobj = apps.get_model('auth', 'user')

    User.objects.create_superuser('superuser','is131322@fhstp.ac.at','superuser')
    user = userobj.objects.get(id=1)

    User.objects.create_user('testuser','is131322@fhstp.ac.at','testuser')
    user = userobj.objects.get(id=2)


    entry(
        user_id=user,
        title='Erster Eintrag',
        description='1. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()
    entry(
        user_id=user,
        title='Zweiter Eintrag',
        description='2. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()
    entry(
        user_id=user,
        title='Dritter Eintrag',
        description='3. Eintrag',
        due_date=datetime.now(),
        due_time=datetime.now()).save()



class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_auto_20150412_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(verbose_name=b'Due Time', blank=True),
        ),
    ]

