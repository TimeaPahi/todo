# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0004_remove_entry_creation_datetime'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='creation_datetime',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 19, 21, 18, 30, 454527, tzinfo=utc), verbose_name=b'Creation Time', auto_now_add=True),
            preserve_default=False,
        ),
    ]
