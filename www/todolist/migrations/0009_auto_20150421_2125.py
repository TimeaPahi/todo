# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0008_auto_20150421_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='due_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(null=True),
        ),
    ]
