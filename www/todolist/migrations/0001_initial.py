# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(name='Entry', fields=[
               ('id', models.AutoField(serialize=False, primary_key=True)),
               ('title', models.TextField(max_length=35)),
               ('description', models.TextField(max_length=150)),
               ('due_date', models.DateField(verbose_name=b'Due date',
                                             blank=True)),
               ('due_time', models.DateField(verbose_name=b'Due Time',
                                             blank=True)),
               ('creation_datetime',
                models.DateTimeField(
                                    default=datetime.date.today,
                                    verbose_name=b'Creation Time')),
               ('done', models.BooleanField(default=False)),
               ('user_id', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
