# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0005_entry_creation_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='due_date',
            field=models.DateField(verbose_name=b'Due date'),
        ),
        migrations.AlterField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(verbose_name=b'Due Time'),
        ),
    ]
