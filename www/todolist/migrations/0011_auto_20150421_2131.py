# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0010_auto_20150421_2129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='due_date',
            field=models.DateField(verbose_name=b'Due Date'),
        ),
        migrations.AlterField(
            model_name='entry',
            name='due_time',
            field=models.TimeField(verbose_name=b'Date Time'),
        ),
    ]
