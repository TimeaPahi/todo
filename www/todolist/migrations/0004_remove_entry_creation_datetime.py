# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0003_auto_20150415_1015'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='creation_datetime',
        ),
    ]
