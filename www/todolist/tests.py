from django.core.urlresolvers import reverse, resolve
from django.test import TestCase
from django.test.client import Client, RequestFactory
from django.contrib.auth.models import User
from www.todolist.models import Entry
import datetime
from django.utils import timezone

# Test for working test database


class DatabaseTests(TestCase):

    def test_entries(self):
        # check datamigration > 3 Entries
        self.assertEqual(Entry.objects.all().count(), 3)
        # check for 2 users
        self.assertEqual(User.objects.all().count(), 2)

# Test for Fixtures


class TodoTestCase(TestCase):
    fixtures = ["testsdata.json"]


class FixtureTests(TestCase):

    def test_entries(self):
        # check for fixtures data load
        self.assertEqual(Entry.objects.all().count(), 3)
        # check if fixture created 2 users
        self.assertEqual(User.objects.all().count(), 2)

# Tests for Views


class ViewTests(TestCase):

    def test_overview(self):
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 302)

    def test_add_view(self):
        response = self.client.get(reverse("add"))
        self.assertEqual(response.status_code, 302)

    def test_delete_view(self):
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)

    def test_edit_view(self):
        response = self.client.get(reverse("edit", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 302)

    def test_switch_view(self):
        response = self.client.get(reverse("switch", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 302)

# Test for creating user


class CreateUserTests(TestCase):

    def test_CreateUser(self):
        # check the original 2 users
        self.assertEqual(User.objects.all().count(), 2)
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='newuser',
            email='newuser@gmail.com',
            password='newuser')
        # check the creation
        self.assertEqual(User.objects.all().count(), 3)

# Tests for URLs


class UrlTests(TestCase):

    def test_add_url(self):
        resolver = resolve('/todolist/add')
        self.assertEqual(resolver.view_name, 'add')

    def test_delete_url(self):
        resolver = resolve('/todolist/login')
        self.assertEqual(resolver.view_name, 'django.contrib.auth.views.login')

    def test_logout_url(self):
        resolver = resolve('/todolist/logout')
        self.assertEqual(resolver.view_name, 'logout')

    def test_edit_url(self):
        resolver = resolve('/todolist/edit/1')
        self.assertEqual(resolver.view_name, 'edit')

    def test_swith_url(self):
        resolver = resolve('/todolist/switch/1')
        self.assertEqual(resolver.view_name, 'switch')

    def test_todolist_url(self):
        resolver = resolve('/todolist/')
        self.assertEqual(resolver.view_name, 'overview')

# Tests for Logins


class InvalidLoginTest(TestCase):

    def test_invalid_login1(self):
        self.client = Client()
        self.client.login(username="LogTry", password="1234")
        response = self.client.get('/todolist/login')
        self.assertEqual(response.status_code, 200)
        # 200 > Login Failed > Login again on the same site

    def test_invalid_login2(self):
        self.client = Client()
        self.client.login(username="LogTry", password="1234")
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 302)
        # 302 > Redirect to Login


class LoginTest(TestCase):

    def test_valid_login(self):
        self.client = Client()
        self.client.login(username="testuser", password="testuser")
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 200)
        # after valid check in OK
        # test overview.html end > logut
        self.assertTrue(response.content.endswith('logout</a>'))

# Test for Logout


class Logout(TestCase):

    def test_valid_login1(self):
        self.client = Client()
        self.client.login(username="testuser", password="testuser")
        # click Logout
        response = self.client.get('/todolist/logout')
        self.assertEqual(response.status_code, 302)
        # 302 > Redirect > to the site login
        response = self.client.get('/todolist/login')
        self.assertEqual(response.status_code, 200)
        # test login.html start > in that case no sense > same start
        self.assertTrue(response.content.startswith('<!DOCTYPE html>'))

# Tests for changing database entries


class Database(TestCase):

    def test_datachange(self):
        entry = Entry.objects.get(pk=1)
        self.assertEquals(entry.id, 1)
        entry.title = 'Make the tests'
        entry.due_date = '2015-06-06'
        entry.due_time = '11:00'
        entry.save()
        self.assertEquals(entry.title, 'Make the tests')


# Test for ADD
class AddTest(TestCase):
    # setup method is called before each test

    def setUp(self):
        self.client = Client()

    def test_valid_add(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        self.client.get(reverse("add"))
        # Add new entry with valid data
        data = {
            'title': 'NewEntry',
            'description': 'Test',
            'due_date': '2015-01-01',
            'due_time': '12:00'
        }
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(Entry.objects.all().count(), 4)
        # check the Nr of Entries after valid'add' (3+1)

    def test_invalidadd(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        self.client.get(reverse("add"))
        # Add new entry without Date und Time
        data = {
            'title': 'NewEntry2',
            'description': 'Test'
        }
        response = self.client.post(reverse("add"))
        self.assertEqual(Entry.objects.all().count(), 3)
        # check the Nr of Entries after 'add' (3+0)

    def test_invalidadd(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        self.client.get(reverse("add"))
        # Add new entry without Date und Time
        data = {
            'title': 'NewEntry2',
            'description': 'Test',
            'due_date': '18.04.2015',
            'due_time': '23:45'
        }
        response = self.client.post(reverse("add"))
        self.assertEqual(Entry.objects.all().count(), 3)
        # check the Nr of Entries after 'add' (3+0)

    def test_csrf(self):
        self.client = Client(enforce_csrf_checks=True)
        self.client.login(username="testuser",
                          password="testuser")
        data = {
            'title': 'CSRF',
            'description': 'CSRF description!',
            'due_date': '2015-04-17',
            'due_time': '12:00',
        }
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Entry.objects.all().count(), 3)


class AddToOpenList(TestCase):

    def test_add_open_list(self):
        self.assertEqual(Entry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        response = self.client.get('/todolist/')
        self.assertTrue(len(response.context['open_list']) == 3)
        self.client.get(reverse("add"))
        # Add new entry with valid data
        data = {
            'title': 'NewEntry',
            'description': 'Test',
            'due_date': '2015-01-01',
            'due_time': '12:00'
        }
        response = self.client.post(reverse("add"), data=data)
        response = self.client.get('/todolist/')
        self.assertTrue(len(response.context['open_list']) == 4)
        self.assertEqual(Entry.objects.all().count(), 4)

# Test for DELETE


class DeleteTest(TestCase):

    def test_delete_1(self):
        self.assertEqual(Entry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        response = self.client.get('/todolist/')
        self.assertTrue('done_list' in response.context)
        cnt_start = len(response.context['done_list'])
        entry = response.context['done_list']

        self.client.get(reverse('delete', kwargs={'id': 1}))

        response = self.client.get('/todolist/')
        self.assertTrue('done_list' in response.context)
        cnt_end = len(response.context['done_list'])
        self.assertFalse(cnt_start > cnt_end)
        self.assertEqual(Entry.objects.all().count(), 2)

    def test_delete_2(self):
        self.assertEqual(Entry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        response = self.client.get('/todolist/')
        self.client.get(reverse('delete', kwargs={'id': 1}))
        self.assertEqual(Entry.objects.all().count(), 2)
        # Testuser can delete only his entries


# Test delete from other user
class DeleteSecureTest(TestCase):

    def test_deleteFromOtherUser(self):
        self.assertEqual(Entry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="superuser",
                          password="superuser")
        response = self.client.get('/todolist/')
        self.client.get(reverse('delete', kwargs={'id': 1}))
        self.assertEqual(Entry.objects.all().count(), 3)
        # Delete from Other User is not posibble
        # Every User see only his own entries
