from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^$', views.overview, name='overview'),
    url(r'^add', views.add, name='add'),
    url(r'^edit/(?P<id>[0-9]+)$', views.edit, name='edit'),
    url(r'^delete/(?P<id>[0-9]+)$', views.delete, name='delete'),
    url(r'^switch/(?P<id>[0-9]+)$', views.switch, name='switch'),
    url(r'^login/?', 'django.contrib.auth.views.login'),
    url(r'^logout/?', views.logout_user, name='logout'),
]
