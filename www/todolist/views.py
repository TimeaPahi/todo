from django.shortcuts import render, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse
from django.template import loader, Context
from django.template.loader import get_template
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from django import forms
from django.forms.models import ModelForm

from www.todolist.models import Entry
from www.todolist.form import new_form

# Create your views here.
class new_form(forms.ModelForm):

    class Meta:
        model = Entry
        fields = ['title', 'description', 'due_date', 'due_time']

due_time = forms.TimeField(required=False)
due_date = forms.DateField(required=False)

@login_required(login_url='django.contrib.auth.views.login')
def overview(request):
   # open_entries = Entry.objects.filter(user_id=1)
   # context = {'open_entries': open_entries}
   # return render(request, 'overview.html', context)

    entries = get_template('overview.html').render({
        'done_list': Entry.objects.filter(
            user_id=request.user,
            done=True
        ),
        'open_list': Entry.objects.filter(
            user_id=request.user,
            done=False
        ),
    })
    return HttpResponse(entries)


@login_required(login_url='django.contrib.auth.views.login')
def add(request):
    if request.method == 'POST':
        form = new_form(request.POST)

        if form.is_valid():
            user_id = request.user
            title = request.POST.get('title', '')
            desc = request.POST.get('description', '')
            date = request.POST.get('due_date', '')
            time = request.POST.get('due_time', '')

            entry = Entry(
                user_id=user_id,
                title=title,
                description=desc,
                due_date=date,
                due_time=time)
            entry.save()
           

            return HttpResponseRedirect('/todolist')
        else:
            return HttpResponseRedirect('add')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = new_form()

    context = {'form': form, 'object': None}
    return render(request, 'forms.html', context)


@login_required(login_url='django.contrib.auth.views.login')
def edit(request, id):
    entry = get_object_or_404(Entry, pk=id, user_id=request.user)
    if request.method == 'POST':
        form = new_form(request.POST)
        if form.is_valid():
            user_id = request.user
            title = request.POST.get('title', '')
            desc = request.POST.get('description', '')
            date = request.POST.get('due_date', '')
            time = request.POST.get('due_time', '')

            Entry.objects.filter(
                pk=id).update(
                user_id=user_id,
                title=title,
                description=desc,
                due_date=date,
                due_time=time)
            return HttpResponseRedirect('/todolist/')
        else:
            return HttpResponseRedirect('')
    else:
        form = new_form(instance=entry)

    context = {'form': form, 'entry': entry}
    return render(request, 'forms.html', context)


@login_required(login_url='django.contrib.auth.views.login')
def delete(request, id):
    entry = get_object_or_404(Entry, pk=id, user_id=request.user)
    entry.delete()
    return redirect('/todolist/')


@login_required(login_url='django.contrib.auth.views.login')
def switch(request, id):
    try:
        entry = get_object_or_404(Entry, pk=id, user_id=request.user)
        entry.done = not entry.done
        entry.save()
    except Entry.DoesNotExist:
        raise Http404("Cannot switch Entry that does not exist")
    return redirect('/todolist/')


@login_required(login_url='django.contrib.auth.views.login')
def logout_user(request):
    logout(request)
    return redirect('django.contrib.auth.views.login')
