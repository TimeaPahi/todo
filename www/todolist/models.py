from django.db import models

from django.contrib.auth.models import User

from datetime import date

# create class Entry with User_ID, Titel,Description,Due Date,Due Time,
# Creation Time, Status wiht Done


class Entry(models.Model):

    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User)
    title = models.TextField(max_length=35)
    description = models.TextField(max_length=150)
    due_date = models.DateField('Due Date')
    due_time = models.TimeField('Date Time')
    creation_datetime = models.DateTimeField('Creation Time',
                                            auto_now_add=True)
    done = models.BooleanField(default=False)


def __unicode__(self):
    return self.title
